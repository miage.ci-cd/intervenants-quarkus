package org.miage.boundary;

import lombok.AllArgsConstructor;
import org.eclipse.microprofile.metrics.MetricUnits;
import org.eclipse.microprofile.metrics.annotation.Counted;
import org.eclipse.microprofile.metrics.annotation.Gauge;
import org.eclipse.microprofile.metrics.annotation.Metered;
import org.eclipse.microprofile.metrics.annotation.Timed;
import org.jboss.resteasy.annotations.jaxrs.PathParam;
import org.miage.entity.Intervenant;
import org.miage.repository.IntervenantRepository;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.Optional;

@Path("intervenants")
@AllArgsConstructor
@Produces("application/json")
@Consumes("application/json")
public class IntervenantBoundary {

    private final IntervenantRepository repository;

    @GET
    @Counted(name = "getAllCount", description = "How many getAll have been performed")
    @Timed(name = "getAllTimer", description = "Measure how long it takes to retrieve all intervenants", unit = MetricUnits.MILLISECONDS)
    @Metered(name = "getAll", unit = MetricUnits.MINUTES, description = "Monitor getAll methods")
    public Response getAll() {
        return Response.ok(repository.findAll()).build();
    }

    @GET
    @Path("{id}")
    @Counted(name = "getOneCount", description = "How many getOne have been performed")
    @Timed(name = "getOneTimer", description = "Measure how long it takes to retrieve a intervenant", unit = MetricUnits.MILLISECONDS)
    public Response getOne(@PathParam Long id) {
        return repository
                .findById(id)
                .map(intervenant -> Response.ok(intervenant).build())
                .orElse(Response.status(404).build());
    }

    @POST
    @Counted(name = "createCount", description = "How many create have been performed")
    @Timed(name = "createTimer", description = "Measure how long it takes to create a new intervenant", unit = MetricUnits.MILLISECONDS)
    public Response create(Intervenant intervenant) {
        return Response.ok(repository.save(intervenant)).status(201).build();
    }

    @PUT
    @Path("{id}")
    @Counted(name = "updateCount", description = "How many update have been performed")
    @Timed(name = "updateTimer", description = "Measure how long it takes to update a intervenant", unit = MetricUnits.MILLISECONDS)
    public Response update(@PathParam Long id, Intervenant intervenant) {
        var opt = Optional.ofNullable(intervenant);

        if (opt.isEmpty()) {
            return Response.status(400).build();
        }

        if (!repository.existsById(id)) {
            return Response.status(404).build();
        }

        intervenant.setId(id);

        return Response.ok(repository.save(intervenant)).build();
    }

    @DELETE
    @PathParam("{id}")
    @Counted(name = "deleteCount", description = "How many delete have been performed")
    @Timed(name = "deleteTimer", description = "Measure how long it takes to retrieve a intervenant", unit = MetricUnits.MILLISECONDS)
    public Response destroy(@PathParam Long id) {
        var opt = repository.findById(id);

        if (opt.isEmpty()) {
            return Response.status(404).build();
        }

        repository.delete(opt.get());

        return Response.noContent().build();
    }

}
