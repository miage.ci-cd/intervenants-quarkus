package org.miage.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@NoArgsConstructor
@Getter
@Setter
@Table(name = "intervenants")
public class Intervenant {

    @Id
    @GeneratedValue
    private Long id;

    private String nom;

    private String prenom;

    private String commune;

    private String codepostal;

    public Intervenant(String nom, String prenom, String commune, String codepostal) {
        this.nom = nom;
        this.prenom = prenom;
        this.commune = commune;
        this.codepostal = codepostal;
    }

}
